<?php

namespace YiiMediaComponent;

class UploadFileAction extends \CAction
{

	public $uploadUrl;
	public $uploadPath;
	//
	public $maxFileAgeMin = 3600;
	public $allowedExtensions;
	public $sizeLimit;
	public $inputName = 'f';

	public $chunkPath;
	public $checkFiles;

	public function run()
	{

		$uploader = new FileUploader;

		if (null != $this->allowedExtensions)
			$uploader->allowedExtensions = $this->allowedExtensions;

		if (null != $this->sizeLimit)
			$uploader->sizeLimit = $this->sizeLimit;

		if (null != $this->inputName)
			$uploader->inputName = $this->inputName;

		if(empty($this->chunkPath))
			$this->chunkPath = \Yii::getPathOfAlias('application.runtime.uploadImgChunks');

		if (!file_exists($this->chunkPath))
			mkdir($this->chunkPath, 0777, true);

		$uploader->chunksFolder = $this->chunkPath;

		if (!file_exists($this->uploadPath))
			mkdir($this->uploadPath, 0777, true);

		if (is_dir($this->uploadPath) && ($dir = opendir($this->uploadPath))) {

			while (($file = readdir($dir)) !== false) {

				$filePath = $this->uploadPath . DIRECTORY_SEPARATOR . $file;

				if (!is_file($filePath))
					continue;

				if ((filemtime($filePath) < time() - $this->maxFileAgeMin))
					@unlink($filePath);
			}

			closedir($dir);
		}

		$filename = md5(mt_rand() . '_' . time() . '_' . $uploader->getName()) . '.' . pathinfo($uploader->getName(), PATHINFO_EXTENSION);
		$result = $uploader->handleUpload($this->uploadPath, $filename);

		if (isset($result['success']) && (null !== $this->checkFiles && is_callable($this->checkFiles) && !call_user_func($this->checkFiles, $this->uploadPath . DIRECTORY_SEPARATOR . $filename))) {

			@unlink($this->uploadPath . DIRECTORY_SEPARATOR . $filename);
			unset($result['success']);

			throw new UploadException(400, 'File type is invalid');
		} else {

			$result['path'] = $this->uploadUrl;
			$result['uploadName'] = $uploader->getUploadName();
		}

		header('Content-type: application/json');
		echo json_encode($result);
		\Yii::app()->end();
	}

}

