<?php

namespace YiiMediaComponent;

class AppComponent extends \CApplicationComponent
{

	const IMG_OP_CROP = 10;
	const IMG_OP_RESIZE = 20;
	const IMG_OP_CROP_FROM_CENTER = 30;
	const IMG_OP_ADAPTIVE_RESIZE = 40;

	public $basePath;
	public $baseUrl;
	//
	public $origin = 'origin';
	public $cache = 'cache';
	public $tmp = 'tmp';
	//
	public $sizeLimit = 5120000; // 5mb = 1024 * 5000kb
	//
	public $allowedTypes = [
		'jpeg' => IMAGETYPE_JPEG,
		'jpg' => IMAGETYPE_JPEG,
		'png' => IMAGETYPE_PNG,
		'gif' => IMAGETYPE_GIF,
	];
	//
	public $filenameSpaceChar = '_';
	public $fileNumerDelemiter = '__';
	public $dirrectoryLevel = 2;
	public $defaultImageOperation = self::IMG_OP_RESIZE;
	//
	public $watermarkActive = false;
	public $watermarkPath;
	//
	public $presets = [];

	public function init()
	{
		parent::init();

		$this->basePath = \Yii::getPathOfAlias($this->basePath);
	}

	public function getFileList($folder, $aAllowed = [])
	{

		$aFiles = [];

		if (false === ($handle = opendir($folder)))
			return $aFiles;

		while (false !== ($file = readdir($handle)))
			if ($file != '.' && $file != '..' && is_file($folder . DIRECTORY_SEPARATOR . $file) && ([] == $aAllowed || isset($aAllowed[mb_strtolower(pathinfo($file, PATHINFO_EXTENSION))])))
				$aFiles[] = $file;

		closedir($handle);

		return $aFiles;
	}

	public function getRandomFile($folder, $aAllowed = [])
	{

		if ([] !== ($aFiles = $this->getFileList($folder, $aAllowed)))
			return $aFiles[array_rand($aFiles)];

		return null;
	}

	static public function getImageOperationOptions()
	{
		return [
			self::IMG_OP_RESIZE => 'Масштабирование',
			self::IMG_OP_ADAPTIVE_RESIZE => 'Масштабирование с усечением',
			self::IMG_OP_CROP => 'Усечение',
			self::IMG_OP_CROP_FROM_CENTER => 'Усечение от центра',
		];
	}

	public function getBasePath()
	{
		return $this->basePath;
	}

	public function getBaseUrl()
	{
		return $this->baseUrl;
	}

	public function getOriginPath($suffix = null)
	{
		return $this->basePath . DIRECTORY_SEPARATOR . $this->origin['path'] . $suffix;
	}

	public function getOriginUrl($suffix = null)
	{
		return $this->baseUrl . '/' . $this->origin['url'] . $suffix;
	}

	public function getCachePath($suffix = null)
	{
		return $this->basePath . DIRECTORY_SEPARATOR . $this->cache['path'] . $suffix;
	}

	public function getCacheUrl($suffix = null)
	{
		return $this->baseUrl . '/' . $this->cache['url'] . $suffix;
	}

	public function getTmpPath($suffix = null)
	{
		return $this->basePath . DIRECTORY_SEPARATOR . $this->tmp['path'] . $suffix;
	}

	public function getTmpUrl($suffix = null)
	{
		return $this->baseUrl . '/' . $this->tmp['url'] . $suffix;
	}

	public function checkImageType($path)
	{

		$ext = mb_strtolower(pathinfo($path, PATHINFO_EXTENSION));

		return isset($this->allowedTypes[$ext]) && exif_imagetype($path) == $this->allowedTypes[$ext];
	}

	public function getPathByFileName($sFile, $delemiter = DIRECTORY_SEPARATOR, $iDepth = null)
	{

		$path = [];

		if (null === $iDepth)
			$iDepth = $this->dirrectoryLevel;

		if ($iDepth > 0) {
			$sFileBase = mb_strtolower(pathinfo($sFile, PATHINFO_FILENAME));

			if (($iFileBaseLen = mb_strlen($sFileBase)) < $iDepth)
				$iDepth = $iFileBaseLen;

			for ($i = 0; $i < $iDepth; $i++) {
				$path[] = mb_substr($sFileBase, 0, $i + 1);
			}
		}

		return join($delemiter, $path);
	}

	public function getCacheSuffixByParams($aParams)
	{

		$aParams = array_merge(['width' => 0, 'height' => 0, 'op' => 'resize', 'options' => []], $aParams);

		$aParams['width'] = (int)$aParams['width'];
		$aParams['height'] = (int)$aParams['height'];

		$path = (int)$aParams['width'] . 'x' . (int)$aParams['height'];

		switch (true) {

			case($aParams['op'] == self::IMG_OP_CROP): {
				$path .= '_c';
				break;
			}

			case($aParams['op'] == self::IMG_OP_CROP_FROM_CENTER): {
				$path .= '_cfc';
				break;
			}

			case($aParams['op'] == self::IMG_OP_ADAPTIVE_RESIZE): {
				$path .= '_ar';
				break;
			}

			case($aParams['op'] == self::IMG_OP_RESIZE): {
				$path .= '_r';
				break;
			}
			default: {
				throw new \CException('Image operation is undefined.');
			}
		}

		return $path;
	}

	public function getName($sName)
	{

		$clean = self::getTranslit(mb_strtolower(trim($sName)));
		$clean = preg_replace('/[^a-zA-Z0-9_\|\s\-\.]/', '', $clean);
		$clean = trim(preg_replace('/[\/_\|\s\.]+/', $this->filenameSpaceChar, $clean), $this->filenameSpaceChar);

		return $clean;
	}

	public function getNameNext($sName)
	{

		$iNextVal = 2;
		if (preg_match('/^(.+)' . preg_quote($this->fileNumerDelemiter) . '(\d+)$/i', $sName, $r)) {
			$sName = $r[1];
			$iNextVal = (int)$r[2] + 1;
		}

		return $this->getName($sName) . $this->fileNumerDelemiter . $iNextVal;
	}

	public function filePrepare($path, $file, $ext = null)
	{

		$name = $this->getName(pathinfo($file, PATHINFO_FILENAME));

		if (empty($name))
			$name = time();

		if (is_null($ext))
			$ext = pathinfo($file, PATHINFO_EXTENSION);

		if (!empty($ext))
			$ext = '.' . $ext;

		$path = rtrim($path, DIRECTORY_SEPARATOR);
		while (@is_file($path . DIRECTORY_SEPARATOR . $name . $ext)) {
			$name = $this->getNameNext($name);
		}

		return $name . $ext;
	}

	protected static function getTranslit($str)
	{

		static $pattern;

		if (null === $pattern) {
			$pattern = [
				'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ъ', 'ы', 'э',
				'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ъ', 'Ы', 'Э',
				'ж', 'ц', 'ч', 'ш', 'щ', 'ь', 'ю', 'я',
				'Ж', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ю', 'Я',
			];

			array_walk($pattern, function (&$v) {
				$v = '/' . $v . '/u';
			});
		}

		static $replacement = [
			'a', 'b', 'v', 'g', 'd', 'e', 'e', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', '', 'i', 'e',
			'A', 'B', 'V', 'G', 'D', 'E', 'E', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', '', 'I', 'E',
			'zh', 'ts', 'ch', 'sh', 'shch', '', 'yu', 'ya',
			'Zh', 'Ts', 'Ch', 'Sh', 'Shch', '', 'Yu', 'Ya',
		];

		return preg_replace($pattern, $replacement, $str);
	}

	public function getDirList($outerDir, $depthLevel = 0, $refresh = false)
	{

		static $aDirs = [];

		$key = md5($outerDir . '___' . $depthLevel);
		if (!isset($aDirs[$key]) || $refresh) {

			$getDir = function ($in, $depth = 0, $level = 0) use (&$getDir) {

				$out = [];

				if (!is_dir($in))
					return $out;

				$level++;
				$dirs = array_diff(scandir($in), ['.', '..']);

				$in .= DIRECTORY_SEPARATOR;
				foreach ($dirs as $d)
					if (is_dir($in . $d)) {
						$out[$in . $d] = $in . $d;
						if ($depth == 0 || ($level < $depth))
							$out = $out + $getDir($in . $d, $depth, $level);
					}

				return $out;
			};

			$aDirs[$key] = $getDir($outerDir, $depthLevel);
		}

		return $aDirs[$key];
	}

	/**
	 * Возвращает URL сгенерированного или оригинального файла картинки
	 *
	 * @param string $imageFileName имя файла
	 * @param string $entityName корневой каталог поиска файла
	 * @param array $aParams параметрый генерации картинки width, height, op = [crop, cropfromcenter, adaptiveresize, resizepercent, resize], options
	 * @return string
	 */
	public function processImage($sourceImage, $destImage, $aParams = [])
	{

		@mkdir(pathinfo($destImage, PATHINFO_DIRNAME), 0777, true);

		try {
			$oSourceImage = \PhpThumbFactory::create($sourceImage, isset($aParams['options']) ? $aParams['options'] : []);
		} catch (Exception $e) {
			return false;
		}

		switch (true) {

			case($aParams['op'] == self::IMG_OP_CROP): {
				$oSourceImage->crop($aParams['x'], $aParams['y'], $aParams['width'], $aParams['height']);
				break;
			}

			case($aParams['op'] == self::IMG_OP_CROP_FROM_CENTER): {
				$oSourceImage->cropFromCenter($aParams['width'], $aParams['height']);
				break;
			}

			case($aParams['op'] == self::IMG_OP_ADAPTIVE_RESIZE): {
				$oSourceImage->adaptiveResize($aParams['width'], $aParams['height']);
				break;
			}

			case($aParams['op'] == self::IMG_OP_RESIZE): {
				$oSourceImage->resize($aParams['width'], $aParams['height']);
				break;
			}
			default: {
				throw new CException('Image operation is undefined.');
			}
		}

		if ($this->watermarkActive && isset($aParams['watermark']) && (bool)$aParams['watermark'] && !empty($this->watermarkPath) && file_exists($this->watermarkPath))
			$oSourceImage->watermarkExt($this->watermarkPath);

		$oSourceImage->save($destImage);
		@chmod($destImage, 0777);

		return true;
	}

	public function getPreset($key)
	{
		return isset($this->presets[$key]) ? $this->presets[$key] : null;
	}

}