<?php

namespace YiiMediaComponent;

class Behavior extends \CActiveRecordBehavior {

	public $mediaComponent = 'media';
	public $imageAttribute = 'image';
	public $entityName;

	public function getMediaEntityName() {
		return null == $this->entityName ? mb_strtolower(get_class($this->getOwner())) : $this->entityName;
	}

	/**
	 *
	 * @return NMedia
	 */
	public function getMediaComponent() {
		return \Yii::app()->getComponent($this->mediaComponent);
	}

	public function getMediaOriginPath() {
		return $this->getMediaComponent()->getOriginPath(DIRECTORY_SEPARATOR . $this->getMediaEntityName());
	}

	public function getMediaOriginUrl() {
		return $this->getMediaComponent()->getOriginUrl('/' . $this->getMediaEntityName());
	}

	public function getMediaCachedPath() {
		return $this->getMediaComponent()->getCachePath(DIRECTORY_SEPARATOR . $this->getMediaEntityName());
	}

	public function getMediaCachedUrl() {
		return $this->getMediaComponent()->getCacheUrl('/' . $this->getMediaEntityName());
	}

	public function getImageOriginPath() {

		$image = $this->getOwner()->{$this->imageAttribute};
		if (empty($image))
			return null;

		$media = $this->getMediaComponent();

		return $media->getOriginPath(DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, [
				$this->getMediaEntityName(),
				$media->getPathByFileName($image),
				$image,
			]));
	}

	public function getImageOriginUrl() {

		$image = $this->getOwner()->{$this->imageAttribute};
		if (empty($image))
			return null;

		$media = $this->getMediaComponent();

		return $media->getOriginUrl('/' . join('/', [
				$this->getMediaEntityName(),
				$media->getPathByFileName($image, '/'),
				$image,
			]));
	}

	public function getImageCachedPath($aParams = []) {

		$image = $this->getOwner()->{$this->imageAttribute};
		if (empty($image))
			return null;

		$media = $this->getMediaComponent();

		return $media->getCachePath(DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, [
				$this->getMediaEntityName(),
				$media->getCacheSuffixByParams($aParams),
				$media->getPathByFileName($image),
				$image,
			]));
	}

	public function getImageCachedUrl($aParams = []) {

		$image = $this->getOwner()->{$this->imageAttribute};
		if (empty($image))
			return null;

		$media = $this->getMediaComponent();

		return $media->getCacheUrl('/' . join('/', [
				$this->getMediaEntityName(),
				$media->getCacheSuffixByParams($aParams),
				$media->getPathByFileName($image, '/'),
				$image,
			]));
	}

	public function deleteCachedImages() {

		$imgFile = $this->getOwner()->{$this->imageAttribute};

		if (empty($imgFile))
			return;

		$dirSuffix = $this->getMediaComponent()->getPathByFileName($imgFile);

		foreach ($this->getMediaComponent()->getDirList($this->getMediaCachedPath(), 1) as $itm)
			@unlink($itm . DIRECTORY_SEPARATOR . $dirSuffix . DIRECTORY_SEPARATOR . $imgFile);
	}

	public function deleteImages() {

		$imgFile = $this->getOwner()->{$this->imageAttribute};

		if (empty($imgFile))
			return;

		@unlink($this->getImageOriginPath());
		$this->deleteCachedImages();
	}

	public function getThumbnail($params = []) {

		if (is_string($params) && null === ($params = $this->getMediaComponent()->getPreset($params)))
			throw new \CException("Preset '{$params}' in media component config not found.");

		if (!isset($params['defaultImage']) || empty($params['defaultImage']))
			$params['defaultImage'] = null;

		$image = $this->getOwner()->{$this->imageAttribute};
		if (empty($image))
			return $params['defaultImage'];

		$params['width'] = isset($params['width']) ? (int)$params['width'] : 0;
		$params['height'] = isset($params['height']) ? (int)$params['height'] : 0;

		$sourceFile = $this->getImageOriginPath();

		if (0 == $params['width'] && 0 == $params['height'])
			return file_exists($sourceFile) ? $this->getImageOriginUrl() : $params['defaultImage'];

		if (!isset($params['op']))
			$params['op'] = $this->getMediaComponent()->defaultImageOperation;
		elseif (!in_array($params['op'], array_keys($this->getMediaComponent()->getImageOperationOptions())))
			throw new \CException('Image operation is undefined.');

		$targetFile = $this->getImageCachedPath($params);
		$targetUrl = $this->getImageCachedUrl($params);

		if (file_exists($targetFile))
			return $targetUrl;

		if (!file_exists($sourceFile))
			return $params['defaultImage'];

		if ($this->getMediaComponent()->processImage($sourceFile, $targetFile, $params))
			return $targetUrl;

		return $params['defaultImage'];
	}

	public function renameImage($newAlias) {

		$imgFile = $this->getOwner()->{$this->imageAttribute};
		if (empty($imgFile))
			return;

		$media = $this->getMediaComponent();

		$oldImage = $this->getImageOriginPath();

		$this->getOwner()->saveAttributes([
			$this->imageAttribute => $media->getName($newAlias) . '.' . mb_strtolower(pathinfo($oldImage, PATHINFO_EXTENSION)),
		]);

		@mkdir(pathinfo($this->getImageOriginPath(), PATHINFO_DIRNAME), 0777, true);
		@rename($oldImage, $this->getImageOriginPath());

		$dirSuffix = $media->getPathByFileName(pathinfo($oldImage, PATHINFO_BASENAME));
		foreach ($media->getDirList($this->getMediaCachedPath(), 1) as $itm)
			@unlink($itm . DIRECTORY_SEPARATOR . $dirSuffix . DIRECTORY_SEPARATOR . pathinfo($oldImage, PATHINFO_BASENAME));
	}

}
